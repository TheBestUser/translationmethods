package kuzmichev;

import kuzmichev.lexer.Lexer;
import kuzmichev.parser.Parser;

public class Main
{
	public static void main(String[] args)
	{
		Integer result;
		
		for (String str : args)
		{
			Parser parser = new Parser(new Lexer(str));
			
			System.out.print(str + " = ");
			
			result = parser.calculate();
			
			if (result != null)
				System.out.println(result);
		}
	}
}
