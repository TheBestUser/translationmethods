package kuzmichev.exceptions;

public class LogicException extends Exception
{
	public LogicException()
	{
		super();
	}
	
	public LogicException(String str)
	{
		super(str);
	}
}
