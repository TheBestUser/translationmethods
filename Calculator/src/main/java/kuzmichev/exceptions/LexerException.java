package kuzmichev.exceptions;

public class LexerException extends Exception
{
	public LexerException()
	{
		super();
	}
	
	public LexerException(String str)
	{
		super(str);
	}
}
