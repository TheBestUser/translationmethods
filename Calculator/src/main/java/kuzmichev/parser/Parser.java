package kuzmichev.parser;

import kuzmichev.exceptions.LexerException;
import kuzmichev.exceptions.LogicException;
import kuzmichev.lexer.Lexeme;
import kuzmichev.lexer.Lexer;

import static kuzmichev.lexer.LexemeType.*;

public class Parser
{
	private Lexer lexer;
	
	public Parser(Lexer lexer)
	{
		this.lexer = lexer;
	}
	
	public Integer calculate()
	{
		try
		{
			Integer expressionResult = parseExpression();
			
			if(lexer.getLexeme() != null)
				throw new LogicException("Некорректное выражение " + lexer.getLexeme().getCharacter());
			
			return expressionResult;
		}
		catch (LexerException l)
		{
			System.out.println(l.getMessage());
			return null;
		}
		catch (LogicException l)
		{
			System.out.println(l.getMessage());
			return null;
		}
	}
	
	private Integer parseExpression() throws LexerException, LogicException
	{
		Integer result = parseTerm();
		Lexeme lexeme;
		
		while (true)
		{
			lexeme = lexer.getLexeme();
			if (lexeme == null)
				break;
			
			switch (lexeme.type)
			{
			case PLUS:
				lexer.clear();
				result += parseTerm();
				break;
			
			case MINUS:
				lexer.clear();
				result -= parseTerm();
				break;
			
			default:
				return result;
			//		throw new LogicException("Неожиданный символ " + lexeme.getCharacter());
			}
		}
		
		return result;
	}
	
	private Integer parseTerm() throws LexerException, LogicException
	{
		Integer term = parseFactor();
		Lexeme lexeme;
		
		while (true)
		{
			lexeme = lexer.getLexeme();
			if (lexeme == null)
				break;
			
			switch (lexeme.type)
			{
			case MUL:
				lexer.clear();
				term *= parseFactor();
				break;
			
			case DIV:
				lexer.clear();
				term /= parseFactor();
				break;
			
			case RIGHT_BRACKET:
				return term;
			
			default:
				return term;
			}
		}
		
		return term;
	}
	
	private Integer parseFactor() throws LexerException, LogicException
	{
		Integer power = parsePower();
		Lexeme lexeme;
		
		while (true)
		{
			lexeme = lexer.getLexeme();
			if (lexeme == null)
				break;
			
			switch (lexeme.type)
			{
			case POW:
				lexer.clear();
				power = (int) Math.pow(power, parseFactor());
				break;
			
			case RIGHT_BRACKET:
				return power;
			
			default:
				return power;
			}
		}
		
		return power;
	}
	
	private Integer parsePower() throws LexerException, LogicException
	{
		Lexeme power = lexer.getLexeme();
		
		if (power == null)
		{
			throw new LexerException("Ожидалось выражение или atom");
		}
		
		switch (power.type)
		{
		case MINUS:
			lexer.clear();
			Integer atom = parseAtom();
			
			if (atom == null)
				throw new LogicException("Ожидалось выражение или atom после '-'");
			else
				return -atom;
		
		default:
			return parseAtom();
		}
	}
	
	private Integer parseAtom() throws LexerException, LogicException
	{
		Lexeme lexeme = lexer.getLexeme();
		lexer.clear();
		
		if (lexeme == null)
		{
			throw new LogicException("Ожидалось выражение или atom");
		}
		
		switch (lexeme.type)
		{
		case NUM:
			return lexeme.getValue();
		
		case LEFT_BRACKET:
			Integer expression = parseExpression();
			
			if (lexer.getLexeme() != null && lexer.getLexeme().type == RIGHT_BRACKET)
			{
				lexer.clear();
				return expression;
			}
			else
				throw new LogicException("Отсутствует знак ')'");
		
		default:
			throw new LogicException("Ожидалось выражение или atom вместо " + lexeme.getCharacter());
		}
	}
}
