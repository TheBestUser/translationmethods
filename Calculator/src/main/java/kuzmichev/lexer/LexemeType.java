package kuzmichev.lexer;

public enum LexemeType
{
	PLUS, MINUS, MUL, DIV, POW,  LEFT_BRACKET, RIGHT_BRACKET, NUM
}
