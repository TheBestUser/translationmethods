package kuzmichev.lexer;

import kuzmichev.exceptions.LexerException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class Lexer
{
	private Reader reader;
	
	private Lexeme lexeme = null;
	private Lexeme tempLexeme = null;
	
	public Lexer(String string)
	{
		this.reader = new StringReader(string.replaceAll(" ", ""));
	}
	
	public void clear()
	{
		lexeme = null;
	}
	
	public Lexeme getLexeme() throws LexerException
	{
		if (lexeme != null)
		{
			return lexeme;
		}
		else if (tempLexeme != null)
		{
			lexeme = tempLexeme;
			tempLexeme = null;
			
			return lexeme;
		}
		else
		{
			return getNextLexeme();
		}
	}
	
	private Lexeme getNextLexeme() throws  LexerException
	{
		Integer totalInt;
		Character tempChar;
		
		try
		{
			int res = reader.read();
			if (res == -1)
				return null;
			
			tempChar = (char) res;
			if (!Character.isDigit(tempChar))
			{
				lexeme = new Lexeme(tempChar);
				return lexeme;
			}
			else
				totalInt = Character.getNumericValue(tempChar);
			
			while (true)
			{
				res = reader.read();
				if (res == -1)
				{
					lexeme = new Lexeme(totalInt);
					return lexeme;
				}
				
				tempChar = (char) res;
				if (Character.isDigit(tempChar))
				{
					totalInt *= 10;
					totalInt += Character.getNumericValue(tempChar);
				}
				else
				{
					tempLexeme = new Lexeme(tempChar);
					lexeme = new Lexeme(totalInt);
					
					return lexeme;
				}
			}
		}
		catch (IOException e)
		{
			throw new LexerException(e.getMessage());
		}
	}
	
}
