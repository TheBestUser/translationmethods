package kuzmichev.lexer;

import kuzmichev.exceptions.LexerException;

import static kuzmichev.lexer.LexemeType.*;

public class Lexeme
{
	public LexemeType type;
	
	private Integer value = null;
	private Character character = null;
	
	Lexeme(Integer value)
	{
		this.type = NUM;
		this.value = value;
	}
	
	Lexeme(Character character) throws LexerException
	{
		switch (character)
		{
		case '+':
			this.type = PLUS;
			break;
		
		case '-':
			this.type = MINUS;
			break;
		
		case '*':
			this.type = MUL;
			break;
		
		case '/':
			this.type = DIV;
			break;
		
		case '^':
			this.type = POW;
			break;
		
		case '(':
			this.type = LEFT_BRACKET;
			break;
		
		case ')':
			this.type = RIGHT_BRACKET;
			break;
		
		default:
			throw new LexerException("Некоректный символ " + character + "!\n");
		}
		
		this.character = character;
	}
	
	public Integer getValue()
	{
		return value;
	}
	
	public Character getCharacter()
	{
		return character;
	}
}
