package kuzmichev;

import kuzmichev.lexer.Lexer;
import kuzmichev.parser.Parser;
import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationTest
{
	@Test
	void testSum0()
	{
		final String expression = "1+3";
		final Integer answer = 4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum1()
	{
		final String expression = "(1+3)";
		final Integer answer = 4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum2()
	{
		final String expression = "1+-3";
		final Integer answer = -2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum3()
	{
		final String expression = "(1+-3)";
		final Integer answer = -2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum4()
	{
		final String expression = "-1+3";
		final Integer answer = 2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum5()
	{
		final String expression = "(-1+3)";
		final Integer answer = 2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum6()
	{
		final String expression = "-1+-3";
		final Integer answer = -4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum7()
	{
		final String expression = "(-1+-3)";
		final Integer answer = -4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub0()
	{
		final String expression = "1-3";
		final Integer answer = -2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub1()
	{
		final String expression = "(1-3)";
		final Integer answer = -2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub2()
	{
		final String expression = "1--3";
		final Integer answer = 4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub3()
	{
		final String expression = "(1--3)";
		final Integer answer = 4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub4()
	{
		final String expression = "-1-3";
		final Integer answer = -4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub5()
	{
		final String expression = "(-1-3)";
		final Integer answer = -4;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub6()
	{
		final String expression = "-1--3";
		final Integer answer = 2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub7()
	{
		final String expression = "(-1--3)";
		final Integer answer = 2;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul0()
	{
		final String expression = "7*2";
		final Integer answer = 14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul1()
	{
		final String expression = "(7*2)";
		final Integer answer = 14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul2()
	{
		final String expression = "7*-2";
		final Integer answer = -14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul3()
	{
		final String expression = "(7*-2)";
		final Integer answer = -14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul4()
	{
		final String expression = "-7*2";
		final Integer answer = -14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul5()
	{
		final String expression = "(-7*2)";
		final Integer answer = -14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul6()
	{
		final String expression = "-7*-2";
		final Integer answer = 14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul7()
	{
		final String expression = "(-7*-2)";
		final Integer answer = 14;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv0()
	{
		final String expression = "7/2";
		final Integer answer = 3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv1()
	{
		final String expression = "(7/2)";
		final Integer answer = 3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv2()
	{
		final String expression = "7/-2";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv3()
	{
		final String expression = "(7/-2)";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv4()
	{
		final String expression = "-7/2";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv5()
	{
		final String expression = "(-7/2)";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv6()
	{
		final String expression = "-7/-2";
		final Integer answer = 3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv7()
	{
		final String expression = "(-7/-2)";
		final Integer answer = 3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow0()
	{
		final String expression = "7^2";
		final Integer answer = 49;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow1()
	{
		final String expression = "(7^2)";
		final Integer answer = 49;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow2()
	{
		final String expression = "7^-2";
		final Integer answer = 0;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow3()
	{
		final String expression = "(7^-2)";
		final Integer answer = 0;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow4()
	{
		final String expression = "-7^2";
		final Integer answer = 49;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow5()
	{
		final String expression = "(-7^2)";
		final Integer answer = 49;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow6()
	{
		final String expression = "-7^-2";
		final Integer answer = 0;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow7()
	{
		final String expression = "(-7^-2)";
		final Integer answer = 0;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow8()
	{
		final String expression = "-2^3";
		final Integer answer = -8;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow9()
	{
		final String expression = "(-2^3)";
		final Integer answer = -8;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
}
