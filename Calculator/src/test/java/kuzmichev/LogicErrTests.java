package kuzmichev;

import kuzmichev.lexer.Lexer;
import kuzmichev.parser.Parser;
import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogicErrTests
{
	@Test
	void testVoidExpr0()
	{
		final String expression = "";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testVoidExpr1()
	{
		final String expression = "()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testLeftBracket0()
	{
		final String expression = "(";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testRightBracket0()
	{
		final String expression = ")";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testLeftBracket1()
	{
		final String expression = "(1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testRightBracket1()
	{
		final String expression = "1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum0()
	{
		final String expression = "1+";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum1()
	{
		final String expression = "(1+)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum2()
	{
		final String expression = "(1+())";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum3()
	{
		final String expression = "1+()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum4()
	{
		final String expression = "+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum5()
	{
		final String expression = "(+1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum6()
	{
		final String expression = "(()+1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSum7()
	{
		final String expression = "()+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub0()
	{
		final String expression = "1-";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub1()
	{
		final String expression = "(1-)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub2()
	{
		final String expression = "(1-())";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSub3()
	{
		final String expression = "1-()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul0()
	{
		final String expression = "1*";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul1()
	{
		final String expression = "(1*)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul2()
	{
		final String expression = "(1*())";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul3()
	{
		final String expression = "1*()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul4()
	{
		final String expression = "*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul5()
	{
		final String expression = "(*1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul6()
	{
		final String expression = "(()*1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMul7()
	{
		final String expression = "()*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv0()
	{
		final String expression = "1/";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv1()
	{
		final String expression = "(1/)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv2()
	{
		final String expression = "(1/())";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv3()
	{
		final String expression = "1/()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv4()
	{
		final String expression = "/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv5()
	{
		final String expression = "(/1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv6()
	{
		final String expression = "(()/1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDiv7()
	{
		final String expression = "()/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow0()
	{
		final String expression = "1^";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow1()
	{
		final String expression = "(1^)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow2()
	{
		final String expression = "(1^())";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow3()
	{
		final String expression = "1^()";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow4()
	{
		final String expression = "^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow5()
	{
		final String expression = "(^1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow6()
	{
		final String expression = "(()^1)";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPow7()
	{
		final String expression = "()^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSubSum()
	{
		final String expression = "0-+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSubMul()
	{
		final String expression = "0-*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSubDiv()
	{
		final String expression = "0-/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSubPow()
	{
		final String expression = "0-^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSumMul()
	{
		final String expression = "0+*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSumSum()
	{
		final String expression = "0++1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSumDiv()
	{
		final String expression = "0+/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testSumPow()
	{
		final String expression = "0+^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMulMul()
	{
		final String expression = "0**1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMulSum()
	{
		final String expression = "0*+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMulDiv()
	{
		final String expression = "0*/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testMulPow()
	{
		final String expression = "0*^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDivDiv()
	{
		final String expression = "0//1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDivSum()
	{
		final String expression = "0/+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDivMul()
	{
		final String expression = "0/*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testDivPow()
	{
		final String expression = "0/^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPowDiv()
	{
		final String expression = "0^/1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPowSum()
	{
		final String expression = "0^+1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPowMul()
	{
		final String expression = "0^*1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testPowPow()
	{
		final String expression = "0^^1";
		final Integer answer = null;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
}
