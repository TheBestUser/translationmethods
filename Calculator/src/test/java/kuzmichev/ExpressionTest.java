package kuzmichev;

import kuzmichev.lexer.Lexer;
import kuzmichev.parser.Parser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExpressionTest
{
	
	@Test
	void testExpr0()
	{
		final String expression = "1";
		final Integer answer = 1;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr1()
	{
		final String expression = "(1)";
		final Integer answer = 1;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr2()
	{
		final String expression = "-3";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr3()
	{
		final String expression = "(-3)";
		final Integer answer = -3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr4()
	{
		final String expression = "-(-3)";
		final Integer answer = 3;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr5()
	{
		final String expression = "1+2+3+4+5";
		final Integer answer = 15;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr6()
	{
		final String expression = "1+2+3+4*5";
		final Integer answer = 26;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr7()
	{
		final String expression = "1+2+3*4/5";
		final Integer answer = 5;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr8()
	{
		final String expression = "1+3/2*4^5";
		final Integer answer = 1025;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr9()
	{
		final String expression = "1+-5/2*4^5";
		final Integer answer = -2047;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr10()
	{
		final String expression = "1+(-5/2*4)^5";
		final Integer answer = -32767;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr11()
	{
		final String expression = "1+-5/2*4^4";
		final Integer answer = -511;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr12()
	{
		final String expression = "1+(-5/2*4)^4";
		final Integer answer = 4097;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
	@Test
	void testExpr13()
	{
		final String expression = "(1 + - 5 / 3 + ( 2 * 4 ) ) ^ 4";
		final Integer answer = 4096;
		
		Lexer lexer = new Lexer(expression);
		Parser parser = new Parser(lexer);
		
		assertEquals(parser.calculate(), answer);
	}
	
}
